// GitSense Context Config 
//
// - Two slashes "//" starts a comment line
// - Three right arrows ">>>" starts a README block
// - Three left arrows "<<<" closes a README block
// 
// Start of context config JSON object. 
{
    "title": "Releases Context",
    "titleBgcolor": "#2980b9",

    // We don't want to specify the repos because we want to control what branches
    // the context user can see
    "repos":[],

    "branches": [
        "bitbucket:atlassian/aui:master",
        "bitbucket:atlassian/aui:5.10.x",
        "bitbucket:atlassian/aui:5.8.x",
        "gitlab:gitlab-org/gitlab-ce:master",
        "gitlab:gitlab-org/gitlab-ee:master",
        "gitlab:gitlab-org/gitlab-ce:8-9-stable",
        "gitlab:gitlab-org/gitlab-ee:8-9-stable-ee",
        "github:atom/atom:master",
        "github:atom/atom:1.9-releases",
        "github:atom/atom:1.8-releases",
        "github:facebook/react:master",
        "github:facebook/react:15-dev",
        "github:facebook/react:15-stable"
    ],

    // Create one click queries for the various releases
    "queries": [
        {
            "title": "Atlassian AUI",
            "branches": [
                "bitbucket:atlassian/aui:master",
                "bitbucket:atlassian/aui:5.10.x",
                "bitbucket:atlassian/aui:5.8.x"
            ],
            "query": ""
        },
        {
            "title": "Atom",
            "branches": [
                "github:atom/atom:master",
                "github:atom/atom:1.9-releases",
                "github:atom/atom:1.8-releases"
            ]
        },
        {
            "title": "GitLab CE and GitLab EE",
            "branches": [
                "gitlab:gitlab-org/gitlab-ce:master",
                "gitlab:gitlab-org/gitlab-ce:8-9-stable",
                "gitlab:gitlab-org/gitlab-ee:master"
            ]
        },
        {
            "title": "React Framework",
            "branches": [
                "github:facebook/react:master",
                "github:facebook/react:15-dev",
                "github:facebook/react:15-stable"
            ]
        }
    ]
}
>>>
<h1>Releases Context Example</h1>
<p>
In this context, we highlight how you can use context "Queries", to help select different 
products and releases for browsing.&nbsp; However, if you prefer, you can always add the links
to the README.
<p>
It is also important to note, how this context only includes latest release branches.&nbsp; If you
haven't already done so, click on the branches button above, to see what we mean.&nbsp; 
By restricting what branches the user can see in this context, we eliminate the need for them to have 
to sift through inactive/irrelevant release branches.
<p>
<ul style="margin-left:20px">
    <li style="line-height:1.7;">
        <a href=https://gitsense.com/insight?c=bitbucket:gitsense/contexts:gs_contexts::releases.ccf#b=github:atom/atom:master::github:atom/atom:1.9-releases::github:atom/atom:1.8-releases style="white-space:nowrap;">Atom Editor</a>
    </li>
    <li style="line-height:1.7;">
        <a href=https://gitsense.com/insight?c=bitbucket:gitsense/contexts:gs_contexts::releases.ccf#b=bitbucket:atlassian/aui:master::bitbucket:atlassian/aui:5.9.x::bitbucket:atlassian/aui:5.8.x style="white-space:nowrap;">Atlassian AUI</a>
    </li>
    <li style="line-height:1.7;">
        <a href=https://gitsense.com/insight?c=bitbucket:gitsense/contexts:gs_contexts::releases.ccf#b=gitlab:gitlab-org/gitlab-ce:master::gitlab:gitlab-org/gitlab-ee:master style="white-space:nowrap;">GitLab CE and GitLab EE</a>
    </li>
    <li style="line-height:1.7;">
        <a href=https://gitsense.com/insight?c=bitbucket:gitsense/contexts:gs_contexts::releases.ccf#b=github:facebook/react:master::github:facebook/react:15-dev::github:facebook/react:15-stable style="white-space:nowrap;">React Framework</a><br>
    </li>
</ul>

<h3>Config source file</h3>
<p>
<a href=https://bitbucket.org/gitsense/contexts/src/gs_contexts/releases.ccf>https://bitbucket.org/gitsense/contexts/src/gs_contexts/releases.ccf</a>
<<< 
